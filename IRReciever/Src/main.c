/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
uint32_t               uwT3IC1Value1 = 0;
uint32_t               uwT3IC1Value2 = 0;
uint32_t               uwDiffCapture1 = 0;

uint32_t               uwT3IC2Value1 = 0;
uint32_t               uwT3IC2Value2 = 0;
uint32_t               uwDiffCapture2 = 0;

uint32_t               uwT3IC4Value1 = 0;
uint32_t               uwT3IC4Value2 = 0;
uint32_t               uwDiffCapture3 = 0;

uint32_t               uwT1IC2Value1 = 0;
uint32_t               uwT1IC2Value2 = 0;
uint32_t               uwDiffCapture4 = 0;

uint32_t               uwT1IC3Value1 = 0;
uint32_t               uwT1IC3Value2 = 0;
uint32_t               uwDiffCapture5 = 0;

/* Capture index */
uint16_t               uhCaptureIndex[5]={0,0,0,0,0};

/* Frequency Value */
uint32_t               uwFrequency[5] = {0,0,0,0,0};

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM1_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM3_Init();
  MX_TIM1_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
	uint8_t data_0[]="Start...\r\n";
	HAL_UART_Transmit(&huart1, data_0, sizeof(data_0), 10);
//	HAL_TIM_IC_Start_IT(&htim3, TIM_CHANNEL_1) ;
//	HAL_TIM_IC_Start_IT(&htim3, TIM_CHANNEL_2);	
	HAL_TIM_IC_Start_IT(&htim3, TIM_CHANNEL_4);
//	HAL_TIM_IC_Start_IT(&htim1, TIM_CHANNEL_2);	
//	HAL_TIM_IC_Start_IT(&htim1, TIM_CHANNEL_3);	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

    /* USER CODE END WHILE */
		HAL_GPIO_TogglePin(GPIOA,GPIO_PIN_14);
		HAL_Delay(250);		
    /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_IC_InitTypeDef sConfigIC = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 0xFFFF;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_IC_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 0;
  if (HAL_TIM_IC_ConfigChannel(&htim1, &sConfigIC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_IC_ConfigChannel(&htim1, &sConfigIC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_IC_InitTypeDef sConfigIC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 0xFFFF;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_IC_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 0;
  if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_14, GPIO_PIN_RESET);

  /*Configure GPIO pin : PA14 */
  GPIO_InitStruct.Pin = GPIO_PIN_14;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
 void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
 {
  if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)
  {
				
    if(uhCaptureIndex == 0)
    {
      /* Get the 1st Input Capture value */
      uwT3IC1Value1 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);
      uhCaptureIndex[0] = 1;
    }
    else if(uhCaptureIndex[0] == 1)
    {
			HAL_TIM_IC_Stop_IT(htim, TIM_CHANNEL_1);			
      /* Get the 2nd Input Capture value */
      uwT3IC1Value2 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1); 

      /* Capture computation */
      if (uwT3IC1Value2 > uwT3IC1Value1)
      {
        uwDiffCapture1 = (uwT3IC1Value2 - uwT3IC1Value1); 
      }
      else if (uwT3IC1Value2 < uwT3IC1Value1)
      {
        /* 0xFFFF is max TIM2_CCRx value */
        uwDiffCapture1 = ((0xFFFF - uwT3IC1Value1) + uwT3IC1Value2) + 1;
      }
      else
      {
        /* If capture values are equal, we have reached the limit of frequency
           measures */
        Error_Handler();
      }
      /* Frequency computation: for this example TIMx (TIM2) is clocked by
         APB1Clk */      
      uwFrequency[0] = HAL_RCC_GetPCLK1Freq() / uwDiffCapture1;
			
			uint8_t data_1[10];
			sprintf(data_1,"%d\r\n",uwFrequency[0]);
			HAL_UART_Transmit(&huart1, data_1, 10, 10);			
      uhCaptureIndex[0] = 0;
			HAL_TIM_IC_Start_IT(htim, TIM_CHANNEL_1);			
    }
  }
	
  if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
  {
		if(htim == &htim3)
		{		
			if(uhCaptureIndex[1] == 0)
			{
				/* Get the 1st Input Capture value */
				uwT3IC2Value1 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2);
				uhCaptureIndex[1] = 1;
			}
			else if(uhCaptureIndex[1] == 1)
			{
				HAL_TIM_IC_Stop_IT(htim, TIM_CHANNEL_2);			
				/* Get the 2nd Input Capture value */
				uwT3IC2Value2 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2); 

				/* Capture computation */
				if (uwT3IC2Value2 > uwT3IC2Value1)
				{
					uwDiffCapture2 = (uwT3IC2Value2 - uwT3IC2Value1); 
				}
				else if (uwT3IC2Value2 < uwT3IC2Value1)
				{
					/* 0xFFFF is max TIM2_CCRx value */
					uwDiffCapture2 = ((0xFFFF - uwT3IC2Value1) + uwT3IC2Value2) + 1;
				}
				else
				{
					/* If capture values are equal, we have reached the limit of frequency
						 measures */
					Error_Handler();
				}
				/* Frequency computation: for this example TIMx (TIM2) is clocked by
					 APB1Clk */      
				uwFrequency[1] = HAL_RCC_GetPCLK1Freq() / uwDiffCapture2;
				
				uint8_t data_1[10];
				sprintf(data_1,"%d\r\n",uwFrequency[1]);
				HAL_UART_Transmit(&huart1, data_1, 10, 10);			
				uhCaptureIndex[1] = 0;
				HAL_TIM_IC_Start_IT(htim, TIM_CHANNEL_2);			
			}
		}
		else if (htim == &htim1)
		{
			if(uhCaptureIndex[3] == 0)
			{
				/* Get the 1st Input Capture value */
				uwT1IC2Value1 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2);
				uhCaptureIndex[3] = 1;
			}
			else if(uhCaptureIndex[3] == 1)
			{
				HAL_TIM_IC_Stop_IT(htim, TIM_CHANNEL_2);			
				/* Get the 2nd Input Capture value */
				uwT1IC2Value2 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2); 

				/* Capture computation */
				if (uwT1IC2Value2 > uwT1IC2Value1)
				{
					uwDiffCapture4 = (uwT1IC2Value2 - uwT1IC2Value1); 
				}
				else if (uwT1IC2Value2 < uwT1IC2Value1)
				{
					/* 0xFFFF is max TIM2_CCRx value */
					uwDiffCapture4 = ((0xFFFF - uwT1IC2Value1) + uwT1IC2Value2) + 1;
				}
				else
				{
					/* If capture values are equal, we have reached the limit of frequency
						 measures */
					Error_Handler();
				}
				/* Frequency computation: for this example TIMx (TIM2) is clocked by
					 APB1Clk */      
				uwFrequency[3] = HAL_RCC_GetPCLK1Freq() / uwDiffCapture4;
				
				uint8_t data_1[10];
				sprintf(data_1,"%d\r\n",uwFrequency[3]);
				HAL_UART_Transmit(&huart1, data_1, 10, 10);			
				uhCaptureIndex[3] = 0;
				HAL_TIM_IC_Start_IT(htim, TIM_CHANNEL_2);			
			}			
		}
	}
  if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_3)
  {
				
    if(uhCaptureIndex[4] == 0)
    {
      /* Get the 1st Input Capture value */
      uwT1IC3Value1 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3);
      uhCaptureIndex[4] = 1;
    }
    else if(uhCaptureIndex[4] == 1)
    {
			HAL_TIM_IC_Stop_IT(htim, TIM_CHANNEL_3);			
      /* Get the 2nd Input Capture value */
      uwT1IC3Value2 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3); 

      /* Capture computation */
      if (uwT1IC3Value2 > uwT1IC3Value1)
      {
        uwDiffCapture5 = (uwT1IC3Value2 - uwT1IC3Value1); 
      }
      else if (uwT1IC3Value2 < uwT1IC3Value1)
      {
        /* 0xFFFF is max TIM2_CCRx value */
        uwDiffCapture5 = ((0xFFFF - uwT1IC3Value1) + uwT1IC3Value2) + 1;
      }
      else
      {
        /* If capture values are equal, we have reached the limit of frequency
           measures */
        Error_Handler();
      }
      /* Frequency computation: for this example TIMx (TIM2) is clocked by
         APB1Clk */      
      uwFrequency[4] = HAL_RCC_GetPCLK1Freq() / uwDiffCapture5;
			
			uint8_t data_1[10];
			sprintf(data_1,"%d\r\n",uwFrequency[4]);
			HAL_UART_Transmit(&huart1, data_1, 10, 10);			
      uhCaptureIndex[4] = 0;
			HAL_TIM_IC_Start_IT(htim, TIM_CHANNEL_3);			
    }
  }	
  if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_4)
  {
				
    if(uhCaptureIndex[2] == 0)
    {
      /* Get the 1st Input Capture value */
      uwT3IC4Value1 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4);
      uhCaptureIndex[2] = 1;
    }
    else if(uhCaptureIndex[2] == 1)
    {
			HAL_TIM_IC_Stop_IT(htim, TIM_CHANNEL_4);			
      /* Get the 2nd Input Capture value */
      uwT3IC4Value2 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4); 

      /* Capture computation */
      if (uwT3IC4Value2 > uwT3IC4Value1)
      {
        uwDiffCapture3 = (uwT3IC4Value2 - uwT3IC4Value1); 
      }
      else if (uwT3IC4Value2 < uwT3IC4Value1)
      {
        /* 0xFFFF is max TIM2_CCRx value */
        uwDiffCapture3 = ((0xFFFF - uwT3IC4Value1) + uwT3IC4Value2) + 1;
      }
      else
      {
        /* If capture values are equal, we have reached the limit of frequency
           measures */
        Error_Handler();
      }
      /* Frequency computation: for this example TIMx (TIM2) is clocked by
         APB1Clk */      
      uwFrequency[2] = HAL_RCC_GetPCLK1Freq() / uwDiffCapture3;
			
//			uint8_t data_1[10];
//			sprintf(data_1,"%d\r\n",uwFrequency[2]);
//			HAL_UART_Transmit(&huart1, data_1, 10, 10);			
      uhCaptureIndex[2] = 0;
			HAL_TIM_IC_Start_IT(htim, TIM_CHANNEL_4);			
    }
  }	
 }
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
